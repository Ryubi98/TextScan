## Simple SDL mini code
 
CC=gcc
 
CPPFLAGS= `pkg-config --cflags sdl` -MMD `pkg-config --cflags gtk+-3.0`
CFLAGS= -Wall -Wextra -std=c99 -O3
LDFLAGS=
LDLIBS= `pkg-config --libs sdl` -lSDL_image -lm `pkg-config --libs gtk+-3.0`
 
OBJ= main.o Neural_Network/matrix.o Neural_Network/nn.o Process/ecran.o Train/train.o Segmentation/segmentation_block.o Process/pixel_operations.o Process/img_process.o 
DEP= ${SRC:.o=.d}
 
all: main
 
main: ${OBJ}
 
clean:
	${RM} ${OBJ} ${DEP} *~
	${RM} main
	${RM} *.d
	${RM} Neural_Network/*.d
	${RM} Segmentation/*.d
	${RM} Train/*.d
	${RM} converted.txt
	${RM} Process/*.d
 
-include ${DEP}
 
# END
