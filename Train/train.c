#include <stdio.h>
#include <stdlib.h>

#include "../Neural_Network/nn.h"

void save(struct NeuralNetwork *nn)
{
	FILE *meta = fopen("Train/meta", "w");
	fprintf(meta, "%d %d %d %lf", nn->nbInputs, nn->nbHiddens, nn->nbOutputs, nn->lr);
	fclose(meta);

	FILE *wih = fopen("Train/wih", "w");
	for(int i = 0; i < nn->nbHiddens*nn->nbInputs; i++)
		fprintf(wih, "%lf ", *(nn->wih+i));
	fclose(wih);

	FILE *who = fopen("Train/who", "w");
	for(int i = 0; i < nn->nbOutputs*nn->nbHiddens; i++)
		fprintf(who, "%lf ", *(nn->who+i));
	fclose(who);
}

struct NeuralNetwork* readNet()
{
	struct NeuralNetwork *nn = malloc(sizeof(struct NeuralNetwork));
	
	FILE *meta = fopen("Train/meta", "r");
	fscanf(meta, "%d %d %d %lf", &(nn->nbInputs), &(nn->nbHiddens), &(nn->nbOutputs), &(nn->lr));
	fclose(meta);
		
	nn->wih = malloc(sizeof(double)*nn->nbHiddens*nn->nbInputs);
	FILE *wih = fopen("Train/wih", "r");
	for(int i = 0; i < nn->nbHiddens*nn->nbInputs; i++)
		fscanf(wih, "%lf ", (nn->wih+i));
	fclose(wih);
	
	nn->who = malloc(sizeof(double)*nn->nbOutputs*nn->nbHiddens);
	FILE *who = fopen("Train/who", "r");
	for(int i = 0; i < nn->nbHiddens*nn->nbOutputs; i++)
		fscanf(who, "%lf ", (nn->who+i));
	fclose(who);
	
	return nn;
}
