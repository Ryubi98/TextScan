#ifndef TRAIN_H_
#define TRAIN_H_

#include "../Neural_Network/nn.h"

void save(struct NeuralNetwork *nn);

struct NeuralNetwork* readNet();

#endif
