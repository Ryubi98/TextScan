#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "pixel_operations.h"
//#include "ecran.h"

SDL_Surface* image_to_gray_level(SDL_Surface *img) {
  for(double i = 0; i < img->w; i++){
    for(double j = 0; j < img->h; j++){
      Uint32 pixel = getpixel(img, i, j);
      Uint8 r, b, g;
      SDL_GetRGB(pixel, img->format, &r, &g, &b);
      Uint8 m = 0.3 * r + 0.59 * g + 0.11 * b;
      putpixel(img, i, j, SDL_MapRGB(img->format, m, m, m));
    }
  }
  return img;
}

void histogram(SDL_Surface* img, int histo[]){
  for(unsigned i = 0; i < 256; i++){
      histo[i] = 0;
  }
  for(double i = 0; i < img->w; i++){
    for(double j = 0; j < img->h; j++){
      Uint32 pixel = getpixel(img, i, j);
      Uint8 r, b, g;
      SDL_GetRGB(pixel, img->format, &r, &g, &b);
      int m;
      m = r;
      histo[m]+= 1;
    }
  }
}

int* image_to_white_and_black(SDL_Surface *img) {
  for(double i = 0; i < img->w; i++){
    for(double j = 0; j < img->h; j++){
      Uint32 pixel = getpixel(img, i, j);
      Uint8 r, b, g;
      SDL_GetRGB(pixel, img->format, &r, &g, &b);
      int m = 16;
      putpixel(img, i, j, SDL_MapRGB(img->format, r/m*m, g/m*m, b/m*m));
    }
  }

  int histo[256];
  histogram(img, histo);


  int pixMed = 0;
  int sum = 0;
  int size = img->w*img->h/2;
  while (sum < size){
    sum += histo[pixMed];
    pixMed++;
  }
	

  int *bin = malloc(img->w*img->h*sizeof(int));

  for(int i = 0; i < img->w; i++){
    for(int j = 0; j < img->h; j++){
      Uint32 pixel = getpixel(img, i, j);
      Uint8 r, g, b;
      SDL_GetRGB(pixel, img->format, &r, &g, &b);
      if(r<pixMed-1){
        *(bin+j*img->w + i) = 1;
        putpixel(img, i, j, SDL_MapRGB(img->format, 0, 0, 0));
      }
      else{
        *(bin+j*img->w + i) = 0;
        putpixel(img, i, j, SDL_MapRGB(img->format, 255, 255, 255));
      }
    }
  }
  return bin;
}

SDL_Surface* remove_grain(SDL_Surface* img){
  for(double i = 1; i < img->w-1; i++){
    for(double j = 1; j < img->h-1; j++){
      Uint32 pixel = getpixel(img, i, j);
      Uint8 r, b, g;
      SDL_GetRGB(pixel, img->format, &r, &g, &b);
      if(r==0){
        pixel = getpixel(img, i-1, j);
        SDL_GetRGB(pixel, img->format, &r, &g, &b);
        if(r==255){
          pixel = getpixel(img, i+1, j);
          SDL_GetRGB(pixel, img->format, &r, &g, &b);
          if(r==255){
            pixel = getpixel(img, i, j+1);
            SDL_GetRGB(pixel, img->format, &r, &g, &b);
            if(r==255){
              pixel = getpixel(img, i, j-1);
              SDL_GetRGB(pixel, img->format, &r, &g, &b);
              if(r==255)
                putpixel(img, i, j, SDL_MapRGB(img->format, 255, 255, 255));
            }
          }
        }
      }
    }
  }
  return img;
}

SDL_Surface* mult_img(SDL_Surface* img, double multW, double multH){
  SDL_Surface *div;
  div = SDL_CreateRGBSurface (0, img->w*multW, img->h*multH, 32, 0, 0, 0, 0);
  Uint32 pixel;
  Uint8 r, b, g;
  for(double i = 0; i < img->w; i++){
    for(double j = 0; j < img->h; j++){
      pixel = getpixel(img, i, j);
      SDL_GetRGB(pixel, img->format, &r, &g, &b);
      for(int k = 0; k < multW; k++)
        for(int l = 0; l < multH; l++)
          putpixel(div, i*multW + k, j*multH + l,
                   SDL_MapRGB(div->format, r, g, b));
    }
  }
  return div;
}
