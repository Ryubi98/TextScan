#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>
#include <gtk/gtk.h>

#include "../Process/pixel_operations.h"
#include "segmentation_block.h"
#include "../Neural_Network/nn.h"

struct matrix_image* matrix_create_from_image(int *bin, int w, int h)
{
	struct matrix_image *mat = malloc(sizeof(struct matrix_image));

	mat->size = h * w;
	mat->w = w;
	mat->h = h;

	mat->array = bin;

	return mat;
}

void lines_free(struct Lines *lines)
{
	for(int i = 0; i < lines->nbLines; i++)
		free(*(lines->lines+i));
	free(lines->lines);
}

void list_free(struct List *list)
{
	if(list != NULL)
	{
		list_free(list->next);
		free(list);
	}
}

struct matrix_image* matrix_copy(struct matrix_image *img)
{
	struct matrix_image *mat = malloc(sizeof(struct matrix_image));
	mat->h = img->h;
	mat->w = img->w;
	mat->size = img->size;
	mat->array = calloc(sizeof(int), mat->size);

	for(int i = 0; i < img->size; i++)
		*(mat->array+i) = *(img->array+i);
	
	return mat;
}

int matrix_distance_horizontal(struct matrix_image *img, int x_from)
{
	int x = x_from;
	while(x < img->w && *(img->array+x) != 1)
		x++;

	return x;
}

int matrix_distance_vertical(struct matrix_image *img, int y_from)
{
	int y = y_from;
  while(y < img->h && *(img->array+y) != 1)
  	y ++;

  return y;
}

struct Lines* init_lines(int size)
{
  struct Lines *lines = malloc(sizeof(struct Lines));
  lines->nbLines = 0;
  lines->sizeMoy = 0;
  lines->lines = calloc(sizeof(struct Line*), size);
  
  return lines;
}

struct Line* init_line(int from, int to)
{
  struct Line *line = malloc(sizeof(struct Line));
  line->from = from;
  line->to = to;
  line->skipped = 0;
  line->jump = 0;
  //line->space = 0;
  
  return line;
}

struct List* init_list(char chr)
{
	struct List *list = malloc(sizeof(struct List));
	list->lettre = chr;
	list->next = NULL;
	
	return list;
}

struct List* segment(struct NeuralNetwork *nn, int *bin, int w, int h, char *t)
{
  struct matrix_image *img = matrix_create_from_image(bin, w, h);

  struct Lines *lines = init_lines(10000);
  separateLines(img, lines);
  struct List *text = separateChar(nn, img, lines, t);
	
	free(img);
	lines_free(lines);
	free(lines);
	
	return text;
}

void separateLines(struct matrix_image *img, struct Lines *ls)
{
  int from = 0;
  int to = 0;

  int counting = 0;
  for(int i = 0; i < img->h; i++)
  {
    int white = 1;
    for(int j = 0; j < img->w; j++)
    {
      if(*(img->array+i*img->w+j) == 1)
      {
        white = 0;
        break;
      }
    }
    
    if(counting)
    {
      if(white)
      {
        to = i;
        *(ls->lines+ls->nbLines) = init_line(from, to);
        ls->nbLines += 1;
        counting = 0;
        ls->sizeMoy = (ls->sizeMoy*ls->nbLines + to-from) / (ls->nbLines+1);
      }
    }
    else if(!white)
    {
      from = i;
      counting = 1;
    }
  }

  for(int i = 0; i < ls->nbLines-1; i++)
  {
    from = (*(ls->lines+i))->from;
    to = (*(ls->lines+i))->to;

		int pfrom = (*(ls->lines+i+1))->from;
		if(to-from < ls->sizeMoy / 2 && i+1 < ls->nbLines)
    {
      (*(ls->lines+i+1))->from = (*(ls->lines+i))->from;
      (*(ls->lines+i))->skipped = 1;
    }
		else if (pfrom-to > 4 * ls->sizeMoy / 3)
		{
			(*(ls->lines+i))->jump = 1;
		}
  }
}

struct List* separateChar(struct NeuralNetwork *nn, struct matrix_image *img,
													struct Lines *ls, char *training)
{
	struct List *list = init_list('\0');
	struct List *copy = list;
	int count = 0;

  for(int l = 0; l < ls->nbLines; l++)
  {
    struct Line *line = *(ls->lines+l);
    
    if(line->skipped)
      continue;
		
		int firstTime = 1;
    int counting = 0;
    int from = 0;
    int to = 0;
    int countW = 0;
    for(int i = 0; i < img->w; i++)
    {
      int white = 1;
      for(int j = line->from; j < line->to; j++)
      {
        if(*(img->array+j*img->w+i)==1)
        {
          white = 0;
          break;
        }
      }

      if(counting)
      {
        if(white)
        {
          to = i;
          counting = 0;
          int lf = line->from;
					if(training == NULL)
					{
						char cara =	returnChar(nn, img, from, lf, to-from, line->to-lf);
						list->next = init_list(cara);
						list = list->next;
					}
          else
					{
						int x = from;
						int y = lf;
						int w = to-from;
						int h = line->to-lf;
						double *input = calloc(sizeof(double), nn->nbInputs);
						for(int i = 0; i < w; i++)
						{
							for(int j = 0; j < h; j++)
							{
								*(input+j*w+i) = (double)(*(img->array+(j+y)*img->w+(i+x)));
							}
						}

						double *output = calloc(sizeof(double), nn->nbOutputs);
						char c = *(training+count);
						if(c >= 'A' && c <= 'Z')
							*(output+c-'A') = 1;
						else if(c >= 'a' && c <= 'z')
							*(output+c-'a'+26) = 1;
						else if(c >= '0' && c <= '9')
						{
							*(output+c-'0'+52) = 1;
						}
						else
						{
							switch (c)
							{
								case '.':
									*(output+62) = 1;
									break;
								case ',':
									*(output+63) = 1;
									break;
								case '?':
									*(output+64) = 1;
									break;
								case '!':
									*(output+65) = 1;
									break;
								case ':':
									*(output+66) = 1;
									break;
								case '/':
									*(output+67) = 1;
									break;
								case '\'':
									*(output+68) = 1;
									break;
							}
						}
						train(nn, input, output);
						count++;
						free(output);
						free(input);
					}
					firstTime = 0;
        }
      }
      else if (!white)
      {
        from = i;
        counting = 1;
        if(countW>6 && !firstTime)
        {
        	list->next = init_list('\40');
          list = list->next;
        	
        }
				countW = 0;
      }
			else//not counting and white
			{
				countW++;
			}
    }
    if(line->jump)
    {
    	list->next = init_list('\n');
    	list = list->next;
    }
    list->next = init_list('\n');
    list = list->next;
  }
  
  return copy;
}

char returnChar(struct NeuralNetwork *nn, struct matrix_image *img,
								int x, int y,
								int w, int h)
{
	double *input = calloc(sizeof(double), nn->nbInputs);
	for(int i = 0; i < w; i++)
	{
		for(int j = 0; j < h; j++)
		{
			*(input+j*w+i) = (double)(*(img->array+(j+y)*img->w+(i+x)));
		}
	}

	double *output = query(nn, input);
	double max = *output;
	int indice = 0;
	for(int i = 1; i < 69; i++)
	{
		if(max < *(output+i))
		{
			max = *(output+i);
			indice = i;
		}
	}
	char lettre = '\0';
	if(indice < 26)
		lettre = indice + 'A';
	else if (indice < 52) 
		lettre = indice-26 + 'a';
	else if (indice < 62)
		lettre = indice-52 + '0';
	else
	{
		switch(indice)
		{
			case 62:
				lettre = '.';
				break;
			case 63:
				lettre = ',';
				break;
			case 64:
				lettre = '?';
				break;
			case 65:
				lettre = '!';
				break;
			case 66:
				lettre = ':';
				break;
			case 67:
				lettre = '/';
				break;
			case 68:
				lettre = '\'';
				break;
		}
	}

	free(input);
	free(output);

	return lettre;
}
