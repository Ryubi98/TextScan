# ifndef NN_H_
# define NN_H_

#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <math.h>
#include <time.h>

double sigmoid(double x);

double sigmoidPrime(double x);

double mutate(double x);

struct NeuralNetwork{
	double *wih;
	double *who;

	int nbInputs;
	int nbHiddens;
	int nbOutputs;

	double lr;
};

struct NeuralNetwork* newNN(int nbInputs, int nbHiddens, int nbOutputs,
														double lr);

void neural_mutate(struct NeuralNetwork *nn);

double* query(struct NeuralNetwork *nn, double *inputs);

void train(struct NeuralNetwork *nn, double *inputs, double *targets);

#endif
