//neural_network.c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "nn.h"
#include "matrix.h"

double sigmoid(double x){
  return (1/(1+exp(-x)));
}

double sigmoidPrime(double x){
  double ex = exp(-x);
  return ex/((1+ex*ex));
}

double mutate(double x)
{
	if(((double)rand())/((double)RAND_MAX) < 0.1)
		x += gaussrand() * 0.5;
	return x;
}

struct NeuralNetwork* newNN(int nbInputs, int nbHiddens, int nbOutputs,
														double lr)
{
	struct NeuralNetwork *NN = malloc(sizeof(struct NeuralNetwork));
	NN->wih = malloc(sizeof(double)*nbHiddens*nbInputs);
	NN->who = malloc(sizeof(double)*nbOutputs*nbHiddens);

	NN->nbInputs = nbInputs;
	NN->nbHiddens = nbHiddens;
	NN->nbOutputs = nbOutputs;

	randomize_mat(NN->wih, nbHiddens, nbInputs);
	randomize_mat(NN->who, nbOutputs, nbHiddens);

	NN->lr = lr;

	return NN;
}

void neural_mutate(struct NeuralNetwork *nn)
{
	map_mat(nn->wih, nn->nbHiddens, nn->nbInputs, mutate);
	map_mat(nn->who, nn->nbOutputs, nn->nbHiddens, mutate);
}

double* query(struct NeuralNetwork *nn, double *inputs)
{
	double *hidden = dot_mat(nn->wih, inputs, nn->nbHiddens, nn->nbInputs, 1);
	map_mat(hidden, nn->nbHiddens, 1, sigmoid);
	double *output = dot_mat(nn->who, hidden, nn->nbOutputs, nn->nbHiddens, 1);
	map_mat(output, nn->nbOutputs, 1, sigmoid);
	free(hidden);
	return output;
}

void train(struct NeuralNetwork *nn, double *inputs, double *targets)
{
	double *hidden = dot_mat(nn->wih, inputs, nn->nbHiddens, nn->nbInputs, 1);
	map_mat(hidden, nn->nbHiddens, 1, sigmoid);

	double *output = dot_mat(nn->who, hidden, nn->nbOutputs, nn->nbHiddens, 1);
	map_mat(output, nn->nbOutputs, 1, sigmoid);

	double *oerrors = subtract_mat(targets, nn->nbOutputs, 1, output);

	double *whoT = transpose_mat(nn->who, nn->nbOutputs, nn->nbHiddens);

	double *herrors = dot_mat(whoT, oerrors, nn->nbHiddens, nn->nbOutputs, 1);

	map_mat(output, nn->nbOutputs, 1, sigmoidPrime);
	mult_mat_mat(output, nn->nbOutputs, 1, oerrors);
	mult_mat_single(output, nn->nbOutputs, 1, nn->lr);

	//warning not the same order
	double *hiddenT = transpose_mat(hidden, nn->nbHiddens, 1);

	map_mat(hidden, nn->nbHiddens, 1, sigmoidPrime);
	mult_mat_mat(hidden, nn->nbHiddens , 1, herrors);
	mult_mat_single(hidden, nn->nbHiddens, 1, nn->lr);

	double *deltaWo = dot_mat(output, hiddenT, nn->nbOutputs, 1, nn->nbHiddens);
	add_mat_mat(nn->who, nn->nbOutputs, nn->nbHiddens, deltaWo);
	free(deltaWo);

	double *inputsT = transpose_mat(inputs, nn->nbInputs, 1);
	double *deltaWh = dot_mat(hidden, inputsT, nn->nbHiddens, 1, nn->nbInputs);
	add_mat_mat(nn->wih, nn->nbHiddens, nn->nbInputs, deltaWh);
	free(deltaWh);

	free(hidden);
	free(output);
	free(oerrors);
	free(whoT);
	free(herrors);
	free(hiddenT);
	free(inputsT);
}
