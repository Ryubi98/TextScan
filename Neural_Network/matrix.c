#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "matrix.h"

double* mat(int rows, int cols)
{
	double *mat = malloc(sizeof(double)*rows*cols);
	for(int i = 0; i < rows*cols; i ++)
		*(mat+i) = 0;
	return mat;
}

double gaussrand()
{
	static double U, V;
	static int phase = 0;
	double Z;

	if(phase == 0) {
		U = (rand() + 1.) / (RAND_MAX + 2.);
		V = rand() / (RAND_MAX + 1.);
		Z = sqrt(-2 * log(U)) * sin(2 * PI * V);
	} else
		Z = sqrt(-2 * log(U)) * cos(2 * PI * V);

	phase = 1 - phase;

	return Z;
}

void randomize_mat(double *mat, int rows, int cols)
{
	for(int i = 0; i < rows*cols; i++)
		*(mat+i) = gaussrand();
}

double* transpose_mat(double *mat, int rows, int cols)
{
	double *res = malloc(sizeof(double)*rows*cols);
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < cols; j++)
			*(res+i+j*rows) = *(mat+j+i*cols);
	return res;
}

double* copy_mat(double *mat, int rows, int cols)
{
	double *res = malloc(sizeof(double)*rows*cols);
	for(int i = 0; i < rows*cols; i++)
		*(res+i) = *(mat+i);
	return res;
}

void add_mat_mat(double *mat, int rows, int cols, double *add)
{
	for(int i = 0; i < rows*cols; i++)
		*(mat+i) += *(add+i);
}

void add_mat_single(double *mat, int rows, int cols, double single)
{
	for(int i = 0; i < rows*cols; i++)
		*(mat+i) += single;
}

void mult_mat_mat(double *mat, int rows, int cols, double *mul)
{
	for(int i = 0; i < rows*cols; i++)
		*(mat+i) *= *(mul+i);
}

void mult_mat_single(double *mat, int rows, int cols, double single)
{
	for(int i = 0; i<rows*cols; i++)
		*(mat+i) *= single;
}

void map_mat(double *mat, int cols, int rows, double (*fonc)(double))
{
	for(int i = 0; i < rows*cols; i++)
		*(mat+i) = (*fonc)(*(mat+i));
}

double* subtract_mat(double *mat, int rows, int cols, double *sub)
{
	double* res = malloc(sizeof(double)*rows*cols);
	for(int i = 0; i < rows*cols; i++)
		*(res+i) = *(mat+i) - *(sub+i);
	return res;
}

double* dot_mat(double *mat1, double *mat2, int rows, int cols, int cols2)
{
	double *res = malloc(sizeof(double)*rows*cols2);
	for(int i = 0; i < rows; i++)
	{
		for(int j = 0; j < cols2; j++)
		{
			*(res+j+ i*cols2) = 0;
			for(int t = 0; t < cols; t++)
			{
				*(res+j+ i*cols2) += *(mat1+t+ i*cols) * *(mat2+j+ t*cols2);
			}
		}
	}
	return res;
}
